package pl.sda.michalrys.kolejka2;

public class MyQueue implements MyAbstractQueue {
    private Person person;

    @Override
    public void add(Person person) {
        if (this.person == null) {
            this.person = person;
            return;
        }
        Person previousPerson = this.person;
        this.person = person;
        this.person.setNext(previousPerson);
    }

    @Override
    public void remove(Person person) {
        // if queue is empty
        if (this.person == null) {
            return;
        }

        // if person to remove is last one
        if (this.person.equals(person)) {
            this.person = this.person.getNext();
            return;
        }

        // queue is not empty
        Person tempPerson = this.person;
        while (tempPerson.getNext() != null) {
            if (tempPerson.getNext().equals(person)) {
                tempPerson.setNext(tempPerson.getNext().getNext());
                break;
            }
            tempPerson = tempPerson.getNext();
        }
    }

    @Override
    public void remove(int index) {
        // exceptions
        if(index <= 0 || index > size()) {
            return;
        }

        // remove person
        int indexFromEnd = size() - index;

        // remove last person
        if (indexFromEnd == 0) {
            this.person = (this.person.getNext());
            return;
        }

        Person tempPerson = this.person;
        for (int i = 1; i < indexFromEnd; i++) {
            tempPerson = tempPerson.getNext();
        }
        tempPerson.setNext(tempPerson.getNext().getNext());
        //System.out.println(tempPerson.getNext().getNext()); TODO  jak wskarzę 0, to jest bląd
    }

    @Override
    public int size() {
        int amountOfPerson = 1;
        Person tempPerson = this.person;

        while (tempPerson.getNext() != null) {
            amountOfPerson++;
            tempPerson = tempPerson.getNext();
        }
        return amountOfPerson;
    }

    @Override
    public Person get(int index) {
        // check index
        if (index <= 0 || index > size()) {
            // System.out.println("Wrong id"); lepiej daj exception
            return null;
        }

        // id of person to get from end
        int personToGet = size() - index + 1;

        // get person
        Person tempPerson = this.person;
        for (int i = 1; i < personToGet; i++) {
            tempPerson = tempPerson.getNext();
        }
        return tempPerson;
    }

    @Override
    public String toString() {
        String result = "Queue: ";

        Person personTemp = this.person;

        while (personTemp.getNext() != null) {
            result += personTemp + ", ";
            personTemp = personTemp.getNext();
        }
        result += personTemp.toString() + " -> KASA";

        return result;
    }
}
