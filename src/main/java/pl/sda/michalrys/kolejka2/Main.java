package pl.sda.michalrys.kolejka2;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("1 Jacek", "Kowalski");
        Person person2 = new Person("2 Aro", "Szparo");
        Person person3 = new Person("3 Kacper", "Staszek");
        Person person4 = new Person("4 Jan", "Staszek");
        Person person5 = new Person("5 Olo", "Staszek");

        MyQueue queue = new MyQueue();
        queue.add(person1);
        queue.add(person2);
        queue.add(person3);
        queue.add(person4);
        queue.add(person5);

        System.out.println(queue);
        System.out.println("-- delete person by Person --");
        //queue.remove(person5);
        queue.remove(2);
        System.out.println(queue);

        System.out.println("-- size of queue --");
        System.out.println(queue.size());

        System.out.println("-- get person --");
        System.out.println(queue);
        System.out.println(queue.get(1));

    }

    private static int numberOfPeople(Person person) {
        int numberOfPeople = 0;
        while (person != null) {
            numberOfPeople++;
            person = person.getNext();
        }
        return numberOfPeople;
    }

    private static void removeFirstPerson(Person person) {
        if (person.getNext() == null) {
            return;
        }

        while (person.getNext().getNext() != null) {
            person = person.getNext();
        }
        person.setNext(null);
    }

    private static void printQueue(Person person) {
        while (person != null) {
            System.out.println(person);
            person = person.getNext();
        }
    }

    private static void removeGivenPerson(Person lastPerson, int personNumber){

        int size = numberOfPeople(lastPerson);

        int personToDelete = size - personNumber;

        Person person = lastPerson;
        for (int i = 0; i < personToDelete - 1; i++) {
            person = person.getNext();
        }
        person.setNext(person.getNext().getNext());
    }
}
