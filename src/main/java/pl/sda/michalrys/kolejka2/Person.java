package pl.sda.michalrys.kolejka2;

class Person {
    private String name;
    private String surname;
    private Person next;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public void setNext(Person next) {
        this.next = next;
    }

    public Person getNext() {
        return next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;
        return next != null ? next.equals(person.next) : person.next == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }
}