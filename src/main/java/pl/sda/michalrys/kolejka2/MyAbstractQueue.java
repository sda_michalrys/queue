package pl.sda.michalrys.kolejka2;

public interface MyAbstractQueue {
    void add(Person person);

    /**
     * Optional
     */
    void remove(Person person);

    void remove(int index);

    int size();

    Person get(int index);
//
//    @Override
//    String toString();
}